// SINGLE SMALL VIEW COMPONENTS

var alterClass = function() {
    var ww = document.body.clientWidth;
    var small = 640;
    var medium = 768;

    //adjust pullquote
    if (!$('.component-pullquote').hasClass('align-inline')) {
        if (ww < small) {
            $('.component-pullquote').removeClass('align-aside');
        } else {
            $('.component-pullquote').addClass('align-aside');
        }
    }

    //adjust full width image
    if (ww < medium) {
        $('.single-story .grid-xx img').removeClass('align--full').addClass('align--center touch').unwrap();;

        $('.single-story img.align--left').removeClass().addClass('tleft');
        $('.single-story img.align--right').removeClass().addClass('tright');
        $('.single-story img').addClass('align--center');

    } else if (ww >= small) {
        $('.single-story .grid-xx img.touch').addClass('align--full').removeClass('align--center');
        $('.single-story img.tleft').removeClass('align--center').addClass('align--left');
        $('.single-story img.tright').removeClass('align--center').addClass('align--right');

        if (!$('.single-story img.touch').parent().hasClass('grid-xx')) {
            $('.single-story img.touch').wrap('<div class="grid-xx"></div>');
        }
    }
};
$(window).resize(function(){
    alterClass();
});
//Fire it when the page first loads:"
alterClass();


// READING PROGRESS BAR
var winHeight = $(window).height(),
    docHeight = $(document).height(),
    progressBar = $('progress'),
    max, value;

/* Set the max scrollable area */
max = docHeight - winHeight;
progressBar.attr('max', max);

$(document).on('scroll', function(){
    value = $(window).scrollTop();
    progressBar.attr('value', value);
});