//FEEDBACK FORM
// $('.feedback-open').click(function(){
//     $('.feedback').toggleClass('feedback-active');
// });

$('.feedback').submit(function(ev) {
    // Prevent the form from actually submitting
    ev.preventDefault();

    // Send it to the server
    $.post({
        url: '/contact/confirmation',
        dataType: 'json',
        data: $(this).serialize(),
        success: function(response) {
            if (response.success) {
                $('.thanks').addClass('thanks--active');

                if($('.thanks__error').hasClass('thanks--active')){
                    $('.thanks__error').removeClass('thanks--active');
                }
                $('.thanks__success').addClass('thanks--active');
                document.querySelector('.feedback').reset();
            } else {
                $('.thanks').addClass('thanks--active');

                if($('.thanks__success').hasClass('thanks--active')){
                    $('.thanks__success').removeClass('thanks--active');
                }
                document.querySelector('.thanks__error').innerHTML = `<p>Please fill out all required fields</p>`;
                $('.thanks__error').addClass('thanks--active');
            }
        }
    });
});


