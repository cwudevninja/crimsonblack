// On hover effect for tile images
$('.nav-sections-feature-story').hover(
    function() {
        $(this).find('img').css('transform', 'scale(1.1)');
        $(this).find('.blanket').css('background-color', 'rgba(0,0,0,0)');
    }, function() {
        $(this).find('img').css('transform', 'scale(1)');
        $(this).find('.blanket').css('background-color', 'rgba(0,0,0,.4)');
    }
)
$('.hero').mouseenter(function(){
    $('.hero img').css('transform','scale(1.03)');
 })
 
 $('.hero').mouseleave(function(){
    $('.hero img').css('transform','scale(1)');
 })