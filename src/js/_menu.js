// MENU
// var trigger = $(".open-menu-label");
// var menu = $(".site-header__side-menu");
// $(trigger).click(function () {
//     $(menu).toggleClass('active');
//     $('body').toggleClass('freeze');
//     $('.pipe:nth-child(2)').toggleClass('pipe-two__active');
//     $('.pipe:last-child').toggleClass('pipe-last__active');
//     $('.pipe:first-child').toggleClass('pipe-one__active');
// });

(function () {
    //elements used
    var freezer = document.body;
    var freezerhtml = document.documentElement;
    var trigger = document.querySelector('.open-menu-label');
    var flyout = document.querySelector('.site-header__side-menu');
    var firstchild = document.querySelector('.pipe:first-child');
    var secondchild = document.querySelector('.pipe:nth-child(2)');
    var lastchild = document.querySelector('.pipe:last-child');
    //trigger button aka hamburger event listener

    trigger.addEventListener('click', function (e) {
        e.stopPropagation();
        //if else class toggler. yes there is a method for that, but it can get out of sync in this case
        if (flyout.classList.contains('active')) {
            flyout.classList.remove('active');
            freezer.classList.remove('freeze');
            freezerhtml.classList.remove('freeze');
            //----add your animation class here
            firstchild.classList.remove('pipe-one__active');
            secondchild.classList.remove('pipe-two__active');
            lastchild.classList.remove('pipe-last__active');
        } else {
            flyout.classList.add('active');
            freezer.classList.add('freeze');
            freezerhtml.classList.add('freeze');
            flyout.focus();
            //----add your animation class here
            firstchild.classList.add('pipe-one__active');
            secondchild.classList.add('pipe-two__active');
            lastchild.classList.add('pipe-last__active');
        }
    });
    flyout.addEventListener('click', function (e) {
        e.stopPropagation()
    })
    // trigger event when menu not clicked
    document.body.addEventListener('click', function (e) {
        console.log(e.target)
        if (e.target != flyout && e.target != trigger) {
            flyout.classList.remove('active');
            freezer.classList.remove('freeze');
            freezerhtml.classList.remove('freeze');
            //----add your animation class here
            firstchild.classList.remove('pipe-one__active');
            secondchild.classList.remove('pipe-two__active');
            lastchild.classList.remove('pipe-last__active');
        }

    });

})();